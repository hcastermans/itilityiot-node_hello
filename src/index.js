var express = require('express');

// Constants
var PORT = 80;

// App
var app = express();
app.get('/', function (req, res) {
 console.log("HTTP request:" + req.path);
 res.send('<h2>My message: ' + process.env.MESSAGE+ '</h2>');
});

app.get('/page', function (req, res) {
 console.log("HTTP request:" + req.path);
 res.sendFile('/src/index.html');
});


app.listen(PORT);
console.log('Running on http://localhost:' + PORT);
